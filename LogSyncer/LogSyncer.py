from os import listdir
from os.path import isfile, join
from datetime import datetime

def GetTime_KafkaLog( textLine ):
  # [2019-04-29 20:08:09,855]
  
  dateText = textLine[1:19]
  
  try:
    return datetime.strptime( dateText, "%Y-%m-%d %H:%M:%S" )
  except ValueError:
    return None
      
  
def GetTime_ZookeeperLog( textLine ):
  # 2019-04-29 20:07:12,887
  
  dateText = textLine[0:19]
  
  try:
    return datetime.strptime( dateText, "%Y-%m-%d %H:%M:%S" )
  except ValueError:
    return None
  
def GetTime_productLog( textLine ):
  # 2019-04-29T20:10:10.5182568-05:00
  
  dateText = textLine[:19]
  
  try:
    return datetime.strptime( dateText, "%Y-%m-%dT%H:%M:%S" )
  except ValueError:
    return None


filesToCompare = [ "2019-04-29_Log_Zookeeper.txt", "2019-04-29_Log_DataAgg.txt", "2019-04-29_Log_Kafka.txt" ]


# Look at files in this directory
#localFiles = [ f for f in listdir( "." ) if isfile( join( ".", f ) ) ]

# Have user select files
# print( "COMPARE WHICH FILE(S)?" )
# print( "Local files:" )

# for i in range( len( localFiles ) ):
  # print( "\t", i+1, localFiles[i] )

# index = -1

# while ( index != 0 ):
  # print( "" )
  # print( "Comparing files:" )
  # print( filesToCompare ) 
  # print( "" ) 

  # index = int( input( "Enter an index to add it to the compare list, or 0 to continue: " ) )
  # adjustedIndex = index - 1
  
  # if ( adjustedIndex >= 0 and adjustedIndex < len( localFiles ) ):
    # filesToCompare.append( localFiles[adjustedIndex] )
  

fileLines = []
filePointers = []
fileEnd = []

# Load the files into lists
for fileToLoad in filesToCompare:
  with open( fileToLoad ) as f:
    lines = f.read().splitlines()
    
    # Add all lines from the text file to a list
    fileLines.append( lines )
    
    # Each file pointer starts at the first line
    filePointers.append( 0 )
    
    # Store how long the file line list is for easy access
    fileEnd.append( len( lines ) )
    

# Build the output file
outputFile = open( "combined.html", "w" )

outputFile.write( "<style type='text/css'>" )
outputFile.write( "table { border: solid 1px #aaa; width:100%; table-layout: fixed; font-family: monospace; }" )
outputFile.write( "table tr th { text-align: left; vertical-align: top; }" )
outputFile.write( "table tr { overflow: auto; }" )
outputFile.write( "table tr td { border-bottom: solid 1px #aaa; vertical-align: top; overflow: auto; }" )
outputFile.write( ".odd { background: #fffcd6; }" )
#outputFile.write( ".header { position: fixed; background: #fff36b; }" )
outputFile.write( "tr.first-row td, tr.first-row th { padding-top: 20px; }" )
outputFile.write( "</style> \n\n" );

outputFile.write( "<script>" )
outputFile.write( "function collapse( row ) {" )
outputFile.write( "var rows = document.getElementsByClassName( 'row-' + row );" )
outputFile.write( "console.log( rows );" )
outputFile.write(   "for ( var i = 0; i < rows.length; i++ ) {" )
outputFile.write(     "console.log( rows[i].style.height );" )
outputFile.write(     "rows[i].style.height = '50px';" )
outputFile.write(     "rows[i].style.display = 'none';" )
outputFile.write(   "}" )
outputFile.write( "}" )
outputFile.write( "</script> \n\n" )

outputFile.write( "<table>" )
outputFile.write( "<tr class='header'>" )
outputFile.write( "<th style='width: 3%;'></th>" )
outputFile.write( "<th style='width: 7%;'>TIME</th>" )
for f in filesToCompare:
  outputFile.write( "<th style='width: 30%;'>" + f + "</th>" )
outputFile.write( "</tr>" )

# Group things together by time stamp
logsByTime = {}

# 0 = Zookeeper,  1 = DataAgg,  2 = Kafka

for line in fileLines[0]:
  timeStamp = GetTime_ZookeeperLog( line )
  
  if ( timeStamp != None ):
    if ( timeStamp not in logsByTime ):
      logsByTime[ timeStamp ] = { "zookeeper": [], "product": [], "kafka": [] }
    
    logsByTime[ timeStamp ][ "zookeeper" ].append( line )
    
for line in fileLines[1]:
  timeStamp = GetTime_productLog( line )
  
  if ( timeStamp != None ):
    if ( timeStamp not in logsByTime ):
      logsByTime[ timeStamp ] = { "zookeeper": [], "product": [], "kafka": [] }
    
    logsByTime[ timeStamp ][ "product" ].append( line )
    
for line in fileLines[2]:
  timeStamp = GetTime_KafkaLog( line )
  
  if ( timeStamp != None ):
    if ( timeStamp not in logsByTime ):
      logsByTime[ timeStamp ] = { "zookeeper": [], "product": [], "kafka": [] }
    
    logsByTime[ timeStamp ][ "kafka" ].append( line )

# Build the table based on time stamps

rowIndex = 0

timeKeys = sorted( logsByTime.keys() )

#for time, logs in logsByTime.items():
for time in timeKeys:
  logs = logsByTime[ time ]
  
  if ( rowIndex % 2 == 0 ):
    if ( rowIndex == 0 ):
      outputFile.write( "<tr class='first-row row-" + str( rowIndex ) + "'>" )
    else:
      outputFile.write( "<tr class=' row-" + str( rowIndex ) + "'>" )
  else:
    outputFile.write( "<tr class='odd row-" + str( rowIndex ) + "'>" )
  
  outputFile.write( "<th style='width: 3%;'><button class='collapse' onclick='collapse(" + str( rowIndex ) + ")'>-</button></th>" )
  outputFile.write( "<th>" + str( time ) + "</th>" )
  
  outputFile.write( "<td>" )
  for log in logs["zookeeper"]:
    outputFile.write( "<p>" + log + "</p>" )
  outputFile.write( "</td>" )
  
  
  outputFile.write( "<td>" )
  for log in logs["product"]:
    outputFile.write( "<p>" + log + "</p>" )
  outputFile.write( "</td>" )
  
  
  outputFile.write( "<td>" )
  for log in logs["kafka"]:
    outputFile.write( "<p>" + log + "</p>" )
  outputFile.write( "</td>" )
  
  outputFile.write( "</tr>\n" )
  
  rowIndex += 1

# allFilesDone = False

# while ( allFilesDone == False ):
    
  # outputFile.write( "<tr>" )
  
  # for i in range( len( filePointers ) ):
    
    # # filesToCompare = [ "2019-04-29_Log_Zookeeper.txt", "2019-04-29_Log_DataAgg.txt", "2019-04-29_Log_Kafka.txt" ]

    
    # if ( filePointers[i] < fileEnd[i] ):
      # outputFile.write( "<td>" + fileLines[ i ][ filePointers[i] ] + "</td>" )
    
      # # Move to next line in file
      # filePointers[i] += 1
    
    # else:
      # outputFile.write( "<td></td>" )
  
  # outputFile.write( "</tr>" )
    
  
  # # Check if we're done yet
  # allFilesDone = True
  # for i in range( len( filePointers ) ):
    # # print( "File Pointer " + str( i ) + ": " + str( filePointers[i] )  + "/" + str( fileEnd[i] ) )
    # if ( filePointers[i] < fileEnd[i] ):
      # allFilesDone = False
      # break
      
  # # TEMP
  # if ( i == 10 ):
    # break

# outputFile.write( "</table>" )

# outputFile.close()

# print( "Wrote to output file: combined.html" )
